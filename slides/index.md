---
title: 2022-11 光速進化！Webモダンフロントエンド
marp: true
paginate: true
markdown.marp.enableHtml: true
style: |
  section p, section li {
    font-size: 0.8rem;
  }

  section.primary_section {
    background-image: url("images/mabushii_man.png");
    background-position: right bottom;
    background-repeat: no-repeat;
  }

  section.primary_section h1 {
    font-size: 3.2rem;
    text-shadow: #FFF 0px 0 10px;
  }
  section.primary_section p {
    color: #999;
  }

  section::after {
    content: attr(data-marpit-pagination) ' / ' attr(data-marpit-pagination-total);
  }

  img[alt="uml diagram"] {
    flex: 1;
  }
---

<!-- _class: primary_section -->

# 2022-11 光速進化！Web モダンフロントエンド

2022/11 by Kenji Saitou

![Slides are here](images/qrcode.png)

- スライド: https://ksaitou.gitlab.io/2022-11_lightspeedprogressivefrontend/
- リポジトリ: https://gitlab.com/ksaitou/2022-11_lightspeedprogressivefrontend/

<!-- mermaid.js -->
<script src="https://unpkg.com/mermaid@9.1.7/dist/mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>

---

## 今日話すこと

最近のフロントエンド(Web フロントエンド)開発の知識や手法について、歴史を振り返りながら **キャッチアップ** していきます。

---

## 近頃、こんな感じになっていませんか？

- 最近の JavaScript/TypeScript 中心の画面開発がわからない
  - jQuery で止まっている
    - いや、prototype.js でとまっている
      - いや、メモ帳で HTML を書いた時代で止まっている
  - React などキーワードは聞いたことがあり、気になっている

割と波に乗り遅れた方をネットでも見かけます。

---

## なぜ乗り遅れた感があるのか？ 〜 Web の進化

- Web ＝ 唯一の全世界のみんなの場所
  - みんなの場所 ＝ いろんな人がいろいろ開発資源を持ってくる
  - あらゆる手法があらゆる層の開発者によってテストされる
- Chromium などの成熟
  - 世界有数の工数がかかっているソフトウェアが世界中の PC にインストールされている
  - Google は Web で全てができるといいと思っている
    - ⇔ Web サービス以外がコモディティ化してほしい

---

## なぜ乗り遅れた感があるのか？ 〜 開発手法

- 画面開発において、当初オマケだった JavaScript がメインになってしまったから
  - 今日の JS は Java などと同様にパッケージ管理を宣言的に行い IDE やエディタを使いつつビルドツールを使って開発していきます
  - JavaScript はブラウザの外でも動くし、そこそこ現実的なパフォーマンスが出る
- 画面は他の言語のオマケとして `public/` や `WEB-INF/` フォルダの中でテキストエディタでちまちま編集していくものと思っていたから
  - 昔は `*.jsp` や `*.php` の中で試行錯誤しながらいじれば良かった
- Web が光速進化していて、フロントエンドのメインストリームが二転三転した時期があったから
  - `Backbone.js` `bower` `Grant` `Riot.js` `YUI` `Dojo` は何処へ
  - ただ、もう近いうちに進化はそこまでないと思う

---

## なぜ JavaScript は画面開発で特別な言語なのか？

- 唯一ブラウザで動く標準のプログラミング言語
  - これが最大の理由
  - JavaScript と離れるほど画面開発は辛くなる
  - ブラウザの機能を呼び出すあらゆる API が JavaScript 用にデザインされている
  - 言語としてもそんなに不満のある水準ではない　むしろよくできている方
- 画面の仕掛けも画面を生成するのも全部 JavaScript にすれば安全だから
  - いちいちサニタイジングなど考慮しなくて済むから
    - PHP: `<span><? htmlspecialchars( $userInput ) ?></span>`
    - JavaScript (JSX): `<span>{ userInput }</span>`
  - 画面の仕掛けをサーバ上でコードを一つも変えずに動かすことができる
- バックエンドまで JavaScript で揃えると開発者は JavaScript を知っていれば全て解決する
- 今日の世界ではどうあがいでも JavaScript は特別な言語であり続ける

---

## 本日のトピック

- 歴史を振り返る
  - Web
  - HTML
  - CSS
  - JavaScript
  - その他
- モダンフロントエンド開発
  - ツール編
  - UI ライブラリ編
  - アーキテクチャ編
  - リアル開発編
- まとめ

---

<!-- _class: primary_section -->

# 歴史を振り返る 〜 Web

Web 全般の歴史について

---

## 1990 年代 〜 2000 年前半

### ブラウザ

- Internet Explorer
  - Active X: IE 自体も Windows の COM/OLE 技術と密結合
  - IE6: 後々までずっと引きずる魔のバージョン
  - 今では結局 Edge (Chromium = 実質 Google Chrome)に
- Netscape Navigator
  - 後の Mozilla Firefox に
- マイナーブラウザ
  - Opera
    - 今では Chromium = 実質 Google Chrome に
  - Safari
    - 今では iOS で一気にシェア拡大
    - WebKit = Chromium のフォーク元のエンジン

---

## 1990 年代 〜 2000 年前半

### Web 標準

Web 標準があんまり機能していない ベンダーが強い

- HTML 4 (4.01)
  - XHTML 1.0
- CSS 2.0
  - スタイルシートは 90 年代終盤から普及
  - HTML でバリバリ見栄えを書くのが普通
- ガラケー向けのコンテンツ
  - CHTML (Compact HTML - i-mode はこれ)
  - HDML (EzWeb = KDDI 独自の言語)

---

## 1990 年代 〜 2000 年前半

### JavaScript

- 1995 年に Netscape Navigator で実装されていたのがはじまり
- 立ち位置としては、無くても成り立つオプショナルな存在。要するにただの「おまけ」
  - オフにしている人も少なからずいた
  - そもそもブラウザに JavaScript がない場合も普通にあった
  - これで Web ページの大切な処理を書くというのが信じられないという扱い
- ブラウザごと独自仕様多い
- Web ページで使えるスクリプト言語は JavaScript だけではなかった
  - VBScript
- RIA: Rich Internet Application
  - そもそも JavaScript 以外に選択肢があった
    - Macromedia Flash/Shockwave
    - Java アプレット
    - HTML で頑張る

---

## 1990 年代 〜 2000 年前半

### サーバサイド

- ブログという言葉すら無かった
  - レンタルサーバで自作サイトを置く
  - 自前でサーバを立てる
  - ガラケー向けのサイト
- テクノロジー
  - CGI (Common Gateway Interface)
  - PHP 4, 5
  - ASP
  - Java Servlet / Struts
  - HTML をうまくレンダリングすればいいだけ、みたいなものがメイン
- クッキー

---

## 1990 年代 〜 2000 年前半

## まとめ

- Web 黎明期
- クローズドな独自技術が幅を効かせていた
- 誰も Web 標準を守らない
- Web ＝ 印刷物の延長線上
- データ交換といえば XML

---

## 2000 年代後半

- Web 2.0
  - SNS やブログなど個人の情報発信が Web サービスを経由して行われるように
- JavaScript で複雑なものが作られつつある時代
  - ライブラリ: [prototype.js](http://prototypejs.org/)
  - サービス: Google Maps
- Ajax - エイジャックス - Asynchronous JavaScript and XML
  - 非同期通信でページをリロードせずに新しいコンテンツをロードできるのが衝撃的だった
  - XMLHTTPRequest
    - もともと MSXML (Microsoft の XML 処理ライブラリ) が実装していた COM (ActiveX 経由で呼べる)
    - ページをリロードしなくても Web サーバと任意に HTTP の非同期通信できる機能
      - 別に XML を通信に使う必要はない
- まだ **Adobe** Flash は安泰
  - 末期のガラケーでサポートされていたりした
- インターネットの動画配信が普通に

---

## 2010 年代 前半

- iPhone = スマホ の普及
  - スマホ = PC と同じことができる制限のないモバイル端末
  - Flash をサポートしない！！
    - HTML + JavaScript + CSS でやるしかない
    - Flash 時代の終焉
- [jQuery](https://jquery.com/) 大躍進
  - 今でも使われている JavaScript ライブラリ
  - とにかく面倒な JavaScript の処理を短く書ける
    - `$("input.product_name").val(product.name)`
  - ブラウザ間の差異を吸収してくれる
  - JavaScript でそこそこ難しいものをみんな作るようになった

---

## 2010 年代 前半

- SEO 技術の蔓延
  - Google の検索エンジンにクロールされるために HTML を正しくマークアップする必要がある
- HTML5
  - 現実に即した、後方互換性がある、モダンな HTML 規格
  - 新しい JavaScript や CSS 仕様もひっくるめて「HTML5」というバズワードに
- AltJS が普及しはじめる
  - JavaScript の文法が貧弱なので、もっと便利な言語で書きたい
  - そのままじゃ動かないので JavaScript に変換（トランスパイル）する
  - [CoffeeScript](https://coffeescript.org/) など

---

## 2010 年代 後半

- JavaScript の進化が再開
  - (1999) ECMAScript 3 => (2009) ECMAScript 5 =>
  - (2015) ECMAScript 6 => ここから毎年新バージョン
  - 近代的な文法や非同期(`async`)の機能を追加
- 古い JS しか理解できないブラウザでも理解できるように新しい文法を変換する仕組みも普及
  - [Babel](https://babeljs.io/)
- JavaScript を一線級の言語として扱うためのツール・エコシステムが普及
  - **「JS で大きくて便利で複雑なものを作りたい」**
  - ユニットテストツール
  - トランスパイラ
  - モジュールバンドリング
  - フレームワーク
  - Node.js
- この辺から今のフロントエンド開発の常識が固まってきた感じ
  - **!!おそらくこの辺から分からなくなった人が続出!!**

---

## 2010 年代 後半

- モダンな UI ライブラリの登場
  - React の登場
    - jQuery と真逆の宣言的 UI というパラダイムの登場
  - ほか: Angular, Vue.js
  - 従来の直接ページを書き換えるフレームワーク(jQuery)と考え方が異なる
    - **!!おそらくこの辺から分からなくなった人が続出!!**
- SPA(Single Page Application)の普及
  - 画面は HTML/JS/CSS のみで静的な作成
    - ファットクライアントの作成に近い
  - API によるエンドポイントが普通に
    - JSON フォーマットを送受信する
    - データは API から取得　データの変更も API

---

## 2010 年代 後半

- ブラウザの収斂
  - 古いブラウザの引退: Internet Explorer
  - ブラウザエンジンが Chromium に一本化
    - Microsoft Edge: Chromium
    - Opera: Chromium
    - Safari: Chromium の分岐元
    - Firefox: Gecko
- Google は Chrome でなんでもできるようにしたい

---

## まとめ

- Web は標準技術に収斂しつつある
  - Flash や他プラグイン技術がなくなり、HTML/JS/CSS の時代に
  - デスクトップアプリは役目を終え、Web で全て完結させる方向性
- 逆に JavaScript や CSS が複雑化の一途をたどっている
  - ここらへんをどうにかするのがモダンフロントエンドの課題

---

<!-- _class: primary_section -->

# 歴史を振り返る 〜 HTML

---

## そもそも HTML (Hyper-Text Markup Language) って何？

- [もともとは実験的に WWW をしていた人の間でハイパーテキスト（リンクのあるテキスト）で他の論文を素早く参照するための言語だった](https://www.w3.org/People/Raggett/book4/ch02.html)
- Web が普及するにつれ、利用者が増え、需要も増えて、役割が増していった
  - 個人のサイト、コーポレートサイト
  - ガラケーサイト、スマホサイト、[テレビの d ボタン](https://ja.wikipedia.org/wiki/Broadcast_Markup_Language)、ePub
  - ヘルプファイル
  - 汎用アプリケーションプラットフォームとして
    - もう GUI も HTML で書けばいいんじゃない？みたいなのはある
      - Electron
- 他に選択肢は無かったのか？
  - [Curl](https://www.curl.com/), Flash, Java アプレット, Silverlight → 全部死亡
  - プラグインや追加ソフトがあるものは一切普及しなかった

---

## 現代の HTML は DOM のシリアライゼーションフォーマットである

- ウェブブラウザが HTML を表示しているというのは半分正解で半分間違い
- HTML を読み込み解釈して DOM (Document Object Model) を構築＆表示している
  - あなたがブラウザで見ているのは HTML ではなく DOM である
    - より正確にいうと DOM にスタイルシートなどが混ざった構造物である
  - 極端にいえば HTML は DOM の容れ物に近い
  - もちろんそうじゃないブラウザもあるが、99%のウェブブラウジングではこれが当てはまる
- [DOM](https://www.w3.org/TR/REC-DOM-Level-1/) とは
  - 1998 年に 1.0 から勧告になった HTML/XML を操作できるツリー API 仕様
  - 色々な言語で実装されることを想定している
- なぜこれを意識する必要があるのか？
  - 今の Web アプリ は HTML に書かれた DOM ではなく、JavaScript が後から生成した DOM がメインだから

---

## 仕様の歴史

- (〜1995) 〜 HTML 2.0
  - RFC(IETF) で管理
- (1997) HTML 3.2
  - ここから W3C 管理
- (1997) HTML 4.0 / HTML 4.01
  - かなり長い間使われた
- (2000) XHTML 1.0 / XHTML 1.1 / XHTML 2.0 (死亡)
  - XML として HTML4.01 を再構成していき、XML アプリケーションとして洗練しようとしたが死亡
- (2014) HTML 5 / HMTL 5.1 / HTML 5.2
  - ここから主に WHATWG 管理
  - W3C は仕様をスナップショットとして切り出してリリース
  - HTML 5 は最近の Web テクノロジーを指すバズワードに
- (〜2022) [HTML Living Standard](https://momdo.github.io/html/)
  - W3C による管理は意味を失い、WHATWG に一本化 ([GitHub](https://github.com/whatwg/html/issues))

---

## HTML Living Standard (元 HTML5)

https://html.spec.whatwg.org/multipage/ ([日本語訳](https://momdo.github.io/html/)）

- 後方互換性を見据えた現実的な仕様
- 要素の追加・削除
  - グループ分類の見直し: ブロック要素等 → フローコンテンツ, インライン要素 → フレージングコンテンツ など
  - 追加: `<header>` `<main>` `<section>` `<aside>` `<input type="tel">` 要素など
  - 追加: `data-*` 属性など
  - 削除: フレーム(`<frameset>`)、見栄えに関するもの(`<center>` etc.)、別の要素で間に合うもの(`<acronym>`)
- [WAI-ARIA](https://www.w3.org/WAI/standards-guidelines/aria/)
  - 障碍者へのアクセス性（アクセシビリティ）の大幅強化
- [ビューポート概念の追加](https://developer.mozilla.org/ja/docs/Web/HTML/Viewport_meta_tag)
- 文字コード: UTF-8 以外非推奨

---

## まとめ

- 現代の HTML は静的なページコンテンツの配信媒体というより、アプリケーションもこなせるマルチ媒体になっている
  - 当然 CSS と JavaScript と組み合わさることが前提
  - Visual Studio Code も HTML/CSS/JS でできている
- HTML は依然重要であるが、戦場はサーバが返す HTML ファイルではなくブラウザが HTML を解釈した後の DOM に移っている

### 推薦図書

- [HTML 解体新書 - 仕様から紐解く本格入門](https://www.amazon.co.jp/dp/4862465277/)
  - HTML を仕様からきちんと知りたい人向けには良書

---

<!-- _class: primary_section -->

# 歴史を振り返る 〜 CSS

---

## そもそも CSS (Cascading Style Sheets) って何？

- HTML や XML のいろんなメディア上（紙・画面など）での見栄えを定義する言語
  - XML も見た目を定義できる
  - HTML はブラウザが定義したスタイルシートが最初からかかっていると考えると理解が楽
- HTML や XML では情報の構造を中心に独立的に表現し、その上にスタイルシートを適用するのが基本的な使い方
  - 現実がそうなってないのは知っています

---

## 仕様の歴史

- (1996) CSS 1
- (1998) CSS 2 (2011) CSS 2.1
  - 要素の自由なポジショニング（ `position: absolute` など）
- (2012〜) CSS3
  - 特に一つの仕様ではなく、CSS2 を上書きする形で、いろいろ分割された仕様の集まり
  - [Cascading Style Sheets (CSS) — The Official Definition](https://www.w3.org/TR/CSS/#css)
  - なんか知らない CSS の仕様、全部ここらへん

CSS 2 で止まっている人も多いのでは？

---

## CSS が昔より進化しているポイント

- より複雑なセレクタ
  - `elem:not([attr="value"])`
  - `document.querySelectorAll(".className")` など JavaScript でも使えるようになっている
- [CSS で変数が使えるように](https://developer.mozilla.org/ja/docs/Web/CSS/Using_CSS_custom_properties)
  - `--variable-name: value; property: var(--variable-name);`
- [CSS で計算ができるように](https://developer.mozilla.org/ja/docs/Web/CSS/calc)
  - `width: calc(100% - 200px)`
- [より多くの単位](https://developer.mozilla.org/ja/docs/Learn/CSS/Building_blocks/Values_and_units)
  - `100vh` `1.2rem`
- 自由なレイアウト配置
  - [Flexbox](https://developer.mozilla.org/ja/docs/Learn/CSS/CSS_layout/Flexbox)
  - [Grid](https://developer.mozilla.org/ja/docs/Learn/CSS/CSS_layout/Grids)

---

## CSS が昔より進化しているポイント

- Web フォント: `*.woff` - [Web Open Font Format](https://ja.wikipedia.org/wiki/Web_Open_Font_Format)
  - [例](https://googlefonts.github.io/japanese/)
  - メタデータ(XML) + アウトラインデータ(OpenType or TrueType)
  - 独自書体、アイコンの配布
- レスポンシブデザイン
  - 画面のサイズ（主に横幅）等の条件によって適用する CSS を変えて、メディアごとに最適なレイアウトに自動で切り替える
    - 条件は [メディアクエリ](https://developer.mozilla.org/ja/docs/Learn/CSS/CSS_layout/Media_queries) を使う
    - ナイトモード対応

---

## CSS 方法論

- [BEM](https://getbem.com/): クラス名の命名ルール

```css
.opinions_box {
  /* コンポーネントルート */
}
.opinions_box__view-more {
}
.opinions_box__text-input {
}
.opinions_box__text-input--is-inactive {
  /* .{Block}__{Element}--{Modifier} */
}
```

- 似たもの: [FLOCSS](https://github.com/hiloki/flocss)

---

## [SCSS/Sass](https://sass-lang.com/)

- CSS を代替文法で書いてツールで CSS に変換する

```scss
.style_a {
  // .style_a .style_b { }
  .style_b {
    @include mixin_foobar;
  }
}
```

- Rails の広がりとともに普及
- スタイルを入れ子にして書ける
- スタイルをモジュール化したものをインクルードすることができる
- [PostCSS](https://github.com/postcss/postcss)
  - 更に自由に文法変換のルールを組み合わせられるようなものもある

---

## リセット CSS

- よりサイトの見栄えを厳密かつ統一的にコントロールするために、ブラウザの初期 CSS をスタイルシートが何もかかってない状態にするための CSS
  - 参考: [2022 年、現在の環境に適した CSS リセットのまとめ](https://coliss.com/articles/build-websites/operation/css/css-reset-for-modern-browser.html)
- アンチテーゼとして、 [Normalize.css](https://necolas.github.io/normalize.css/) というのもある
  - こちらは要素ごとのデフォルトの見栄えを、そこそこベターな見た目に統一する目的

---

## CSS フレームワーク - [Bootstrap](https://getbootstrap.jp/)

- デザイナーがいなくても、そこそこの見た目のスタイルシートをすぐに使えるようにしてくれる
  - [例](https://getbootstrap.jp/docs/5.0/forms/form-control/)
- ちゃんとした動作には JavaScript も必要
- かなり定着している　根強いファンも多い

---

## CSS フレームワーク - [Tailwind CSS](https://tailwindcss.com/)

- [下記のようにスタイルシートではなくクラス名でスタイルを与えることができる](https://play.tailwindcss.com/)

```html
<div class="m-2 p-2 rounded bg-blue-300 xl:bg-blue-600">
  マージン2, パディング2で角が丸くて薄青の四角（画面を広げると青が濃くなる）
</div>
```

- **ユーティリティ CSS** という選択肢
- 仕組みとしてはルールに従って大量にクラス名とスタイルが組み合わせで定義されている
  - 本番ビルド時に使っているクラス名だけ残すといったことも可能
- 結局 CSS のクラス名ベースのやり方は以下のような欠点がある
  - 大して一貫したルールが作れない
  - 命名で悩む
  - どうせコンポーネント単位で作り込むのでそこに影響をとどめたい
- こちらのほうが速くコーディングできる場合が多い

---

## まとめ

- CSS は多方面で継続的に強化されている
- 現実の CSS コーディングは様々なモジュール性を求められている
  - 複数人でのサイト構築作業を考えると自然な流れ
  - もともとの CSS の仕様がモジュール性をあまり指向していない
    - あと見栄えというのは常に要求が矛盾しがちではある

## 参考サイト

- [Can I use](https://caniuse.com/)
  - CSS に限らず、ブラウザが何をサポートしているか迷ったらこのサイトを見るのが吉

---

<!-- _class: primary_section -->

# 歴史を振り返る 〜 JavaScript

---

# ECMAScript = JavaScript

- Ecma International で標準仕様を定めている
- [バージョン](https://en.wikipedia.org/wiki/ECMAScript#History)
  - (1999) ECMAScript 3
  - (2009) ECMAScript 5
  - (2016) ES2015 / ECMAScript 6
  - ...
  - (2022) ES2022 / ECMAScript 13
- [各提案ごと 0-4 のステージを成り上がって](https://tc39.es/process-document/)言語仕様に入る
- 別に標準になったからといってブラウザ等に即座に実装されるわけではない
  - 逆にブラウザに実装されてから標準になることもある
- 新しい言語仕様を使いたい人は [Babel](https://babeljs.io/) を使う
  - 古い構文に書き換えてくれる

---

## JavaScript が昔より進化しているポイント 〜 ECMAScript として

- `var` 以外の変数宣言として `let` `const` の導入
- `function(){}` 以外の関数として `() => {}` (アロー関数) の導入
- 非同期(`async` `await` `yield`)の導入（ワンショット継続）
- クラス構文の導入
- スプレッド演算子の導入: `const copiedList = [ ...list]`
- 分割代入: `const { keyname } = object;`
- optional chaining : `foo?.bar?.c` / null coalescing: `foo ?? bar`
- コレクション（`Set` / `Map`）の導入
- 任意精度整数 (`BigInt`)

---

## JavaScript が昔より進化しているポイント 〜 ブラウザ上の API として

一通り JavaScript 上でなんでもできるようになってきている

- 非同期通信: XMLHTTPRequest / fetch
- 履歴操作: History API
- Cookie よりはるかに大きいストレージ: LocalStorage / SessionStorage / IndexedDB
- 描画機能: canvas / WebGL
- 位置情報: Geolocations API
- リアルタイム通信: WebSockets
- ファイル操作
- PWA 関連: Service Worker API
  - バックグラウンド処理(Web Workers)、通信のキャッシュ、プッシュ通知など
- Web Assembly

---

## まとめ

- JavaScript はアプリケーションプラットフォームになれるような言語になっている
  - しかし相変わらずシングルスレッド（UI スレッド = メインスレッド）
- JavaScript で作られたアプリケーションの増加
  - Google なんとか
    - Google は JavaScript に OS 並みに便利になってほしい動機がある
  - 最近 Office もブラウザの画面で動く

---

<!-- _class: primary_section -->

# その他の進化など

---

## その他の進化

- 画像フォーマット: [HEIC](https://ja.wikipedia.org/wiki/High_Efficiency_Image_File_Format), [WebP](https://ja.wikipedia.org/wiki/WebP)
- Web サーバのトレンド変化: Nginx が Apache を抜いて首位に
  - 非常に並列性に優れていて [C10K 問題](https://ja.wikipedia.org/wiki/C10K%E5%95%8F%E9%A1%8C) をクリアできた
- HTTP プロトコル: HTTP/2, HTTP/3 (QUIC)
- TLS 1.3 (HTTPS): TLS 1.1 以下＆SSL はほぼ死亡
- セキュリティ
  - [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS), [CSP](<https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP#:~:text=Content%20Security%20Policy%20(CSP)%20is,site%20defacement%2C%20to%20malware%20distribution.>), [HSTS](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security)
  - HTTPS が当たり前になってる: [HTTPS じゃないと使えない機能](https://developer.mozilla.org/en-US/docs/Web/Security/Secure_Contexts/features_restricted_to_secure_contexts)も出てくるように

---

<!-- _class: primary_section -->

# モダン Web フロントエンド開発 〜 ツール編

今どきの Web 開発に登場する開発ツールについて話します

---

## モダン Web フロントエンド開発とは

- HTML/JS/CSS で複雑な Web アプリケーションを作るための方法論やツール

- 「大きくて複雑な Web アプリを作りたい！！！」
- 「既存の Web 開発を効率化したい！」

（昨今のユーザーの目が肥えてきている）

---

## 逆に最も原始的な開発とは

これをメモ帳で書き換える　**きびしい……**

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>タイトル</title>
    <style>
      /* スタイルシート */
    </style>
  </head>
  <body>
    <script>
      /* JavaScript */
    </script>
  </body>
</html>
```

---

## 既存の何が問題か？

- 便利なツールを動かしたい: Node.js
- 1 ファイルにまとまりすぎ
  - 外部ライブラリを参照したい: npm (yarn)
  - モジュールにわけて開発したい: webpack など
- いろんなブラウザで動かしたい: Babel
- 動かしてみるまできちんと動くか分からない
  - 静的型チェック: TypeScript
  - ユニットテスト: Jest
- コーディング規約: ESLint

人権がほしい ＝ モダンフロントエンド開発

一つ一つどういうツールか見ていきましょう

---

## いろんな便利なツールを動かしたい 〜 [Node.js](https://nodejs.org/ja/)

![width:200px](images/NodeJS.png)

- JavaScript をブラウザ外で動かすためのランタイム
  - 今日の JavaScript 開発で最も重要なツール
  - JavaScript がブラウザだけで動くというのは都合が悪い
    - 何をするにしてもブラウザを立ち上げないとできない
- JavaScript 用のツールは JavaScript で書かれている
- 他にも似たようなものはあるが、 Node.js がデファクトスタンダードになっている
  - 類似: Deno, Bun, GraalJS (, Rhino, Nashorn)
- プロジェクトによってバージョン固定したい場合は [nodenv](https://github.com/nodenv/nodenv) が便利

---

## 外部ライブラリを参照したい 〜 npm

今どきは `<script>` 要素で外部ライブラリを参照しません

```html
<!-- 今どき、こういうのはやらない -->
<script src="/common/jquery.all.js"></script>
```

`package.json` に依存性を書いてライブラリとして DL できます。

```json
{
  "dependencies": {
    "jquery": "3.6.1"
  }
}
```

```bash
# ./node_modules/jquery にライブラリが入る
$ npm install
```

---

## 外部ライブラリを参照したい 〜 npm

- 外部ライブラリは標準リポジトリとして [npm - https://www.npmjs.com/](https://www.npmjs.com/) が用意されている
  - Microsoft が運営

![height:100px](images/npmsite.png)

- 自前で npm 互換のリポジトリを立てることも可能
  - jFrog, Nexus, GitLab Packages, GitHub Packages

---

## 外部ライブラリを参照したい 〜 yarn

![](images/yarn.png)

- [yarn](https://yarnpkg.com/)
  - npm (ツールの方) の代替ツール
  - monorepo 対応や速度の面で Node.js 標準添付の npm より優れているので好まれる傾向がある

---

## 外部ライブラリを参照したい 〜 CommonJS / ESM

- 外部ファイル・外部モジュール・外部ライブラリのパッケージインポート仕様として下記がある

```js
// CommonJS
const { foobar } = require("baz-library");
// ESM(ES Modules)
import foobar from "baz-library";

foobar("今日も一日");
```

- `./node_modules` や `src/` 内などあらかじめ決められた場所からモジュールを検索
- JavaScript 中に上記の文を書くことで他ソースファイル・他モジュールを参照可能
  - JavaScript になっていればなんでも参照可能なので、**CSS や画像ファイルなど非 JavaScript ファイルも参照することも可能**
    - webpack （次ページ）で詳しく

---

## モジュールにわけて開発したい 〜 古いやり方

たくさんの依存関係にある JS・CSS ファイル等を組み合わせたい

```mermaid
graph LR;
  file1.js-->file3.js;
  file3.js-->file4.js;
  file1.js-->file2.js;
  file2.js-->file4.js;
  file2.js-->common.css;
```

古いやり方だと手作業で ↓ これを書く。やりたくない。

```html
<script src="file4.js"></script>
<script src="file3.js"></script>
<link rel="stylesheet" href="common.css" />
<script src="file2.js"></script>
<script src="file1.js"></script>
```

---

## モジュールにわけて開発したい 〜 今のやり方（バンドラ）

```mermaid
graph LR;
  file1.js-->file3.js;
  file3.js-->file4.js;
  file1.js-->file2.js;
  file2.js-->file4.js;
  file2.js-->common.css;
```

各モジュールはモジュールインポートを素直に書く。

```js
// file1.js
require("file3.js");
```

```js
// file2.js
require("file4.js");
require("common.css");
```

```js
// file3.js
require("file4.js");
```

---

## モジュールにわけて開発したい 〜 今のやり方（バンドラ）

```mermaid
graph LR;
  file1.js-->file3.js;
  file3.js-->file4.js;
  file1.js-->file2.js;
  file2.js-->file4.js;
  file2.js-->common.css;
```

**モジュールバンドラ**に下記をお願いする。

- モジュールの依存性解析 (`file1.js -> file3.js` など)
- バンドル (モジュールの結合)

上記の結果作成された下記のバンドル済ファイルをインポートする。

```html
<link rel="stylsheet" href="all_bunbled.css" />
<script src="all_bunbled.js"></script>
```

---

## モジュールにわけて開発したい 〜 [webpack](https://webpack.js.org/)

![width:200px](images/webpack.png)

- 今使われている代表的な **モジュールバンドラ**
  - いろんなモジュール・ライブラリ・スタイルシート・画像などを一つにまとめて成果物として使えるような状態にする
  - 依存性をたどりながらモジュールや画像を変換したり合体させたりする
    - JavaScript じゃないモジュールは JavaScript に変換したりなど
- 代表的なモジュールバンドラ
  - [webpack](https://webpack.js.org/)
    - 長いあいだ使われている代表的なバンドラ　画面はこれが多い
  - [rollup.js](https://rollupjs.org/)
    - ライブラリによく使われているバンドラ　ライブラリはこれが多い
  - [Parcel](https://ja.parceljs.org/)
    - 複雑な設定が要らないバンドラ

---

## いろんなブラウザで動くコードを出したい 〜 [Babel](https://babeljs.io/)

![width:200px](images/babel.png)

古いブラウザでも動く JavaScript を出力するようにする

```javascript
const message = `Hello ${name}!`;
// ↓↓ 古いブラウザでも理解できるように変換 ↓↓
var message = "Hello " + name + "!";
```

Webpack 上で動かして、JavaScript を古いブラウザ互換にしたりなど

---

## 静的型チェックを行いたい 〜 [TypeScript](https://www.typescriptlang.org/)

![width:100px](images/ts.png)

- JavaScript はいわゆる型チェックがない
- ブラウザで動かすまで型のエラー（想定する値の集合の不整合）が分からない

```js
function calcTax(x) {
  return x * 1.1; // 文字列に掛け算できない
}
// こういうのが平気で書けてしまうが、ブラウザで動かすまでエラーと気づかない
calcTax("Hello");
```

```ts
function calcTax(x: number): number {
  return x * 1.1;
}
// Argument of type 'string' is not assignable to parameter of type 'number'.
calcTax("Hello");
```

---

## 静的型チェックを行いたい 〜 [TypeScript](https://www.typescriptlang.org/)

![width:100px](images/ts.png)

- 実用的な言語としては最上レベルの柔軟な型の表現ができるのが特徴

```ts
// "alpha" もしくは "beta" という文字列
type AlphaBet = "alpha" | "beta";
// TypeError
const gamma: AlphaBet = "gamma";
```

- **JavaScript の文法に型の情報を付けただけ** なのが特徴
- 型の情報を捨てれば JavaScript に簡単に変換できる
  - JavaScript のコードが想像しやすい
- 当初は JS 完全互換路線じゃなかった(例: `enum`)が、実用上問題があるので、今は JavaScript 標準から外れた機能には消極的
- **AltJS としてはほぼデファクトになっている**

---

## ユニットテストしたい 〜 [Jest](https://jestjs.io/ja/)

![width:70px](images/jest.png)

- 現状、ほぼデファクトになっているユニットテストツール

```js
export const factorial = (x) => (x <= 0 ? 1 : x * factorial(x - 1));
```

```js
test("10! = 3628800", () => {
  expect(factorial(10)).toEqual(3628800);
});
```

- モックツールなど全てある
- カバレッジも取得できる
- JUnit XML 形式でレポート出したりもできる

---

## この調子で無限にツールを紹介できると思いますが、ここらへんでやめておきます

- とにかくフロントエンドはツールが多い
  - 初学者の挫折の主な原因の一つ
  - 過去はもっと選択肢があったけれども、今は選択肢がある程度決まっている分楽かも
  - ツールが多すぎるので大統一ツールを作るプロジェクトもある: [Rome](https://rome.tools/)
- ほか紹介しきれなかったもの
  - ESLint
    - コードのスタイルチェック (Checkstyle や Findbugs みたいなもの)
  - Prettier
    - コードの整形
  - Visual Studio Code (VSCode)
    - エディタ

---

## 僕は気楽に JavaScript が書きたいだけなんだ！

- ツールの利用は必須ではない
  - 小さいものを作るだけなら不要
- ある程度の規模以上の開発ならきちんとやったほうが良い
  - 最初適当にやったものの保守は予想以上に大変になっていく
- 一瞬で初期セットアップが揃うものもいろいろ配布されている（下記は例）

```bash
# React.js が入った画面プロジェクトの初期セット作成
$ npx create-react-app my-app
```

---

<!-- _class: primary_section -->

# モダン Web フロントエンド開発 〜 UI ライブラリ編

React について話します

---

## jQuery や既存 JS の何がまずかったか

- 別に完全にまずいわけではないが、**命令的なパラダイムで大規模な Web アプリを作るのが難しい**
  - 命令的
    - JS: `document.getElementById("hoge").innerText = "foobar"`
    - JS with jQuery: `$("#hoge").text("foobar")`
- 具体的に何が難しい？
  - UI の状態を管理するのが難しい
    - 「都道府県コンボボックスを選んだら、市区町村をロードする…　んだけど、市区町村のコンボボックスにイベントハンドラを入れて、どこそこの要素のクラス名を書き換えておいて、こっちに別のイベントハンドラを設定して…」 みたいなのが無限に発生する
    - 「この ID の要素が存在するはず」 → ありませんでした！！　が多発する
  - コード追うのが難しい
    - オレオレ UI 管理みたいなのが大量に発生しがち
  - コンポーネント分割が難しい

---

## たとえば、こういうケースを考えてみましょう

![width:300px](images/2022-11-20-16-29-30.png)

- Todo リストアプリ
  - いつでも入力欄から Todo を追加できる
  - 追加順に Todo リストに Todo が並ぶ
  - Todo をクリックすると「処理済」「未処理」を切り替えられる
  - 「処理済を消す」のチェックボックスを押すと処理していないものだけ表示される
  - 検索バーで絞り込むことができる
- jQuery で上記を作ろうとすると、割と泥沼化するのはわかると思います

---

## jQuery

```js
// 注: 私が雰囲気でコーディングしたものです
$(() => {
  $("button#addtodo").on("click", function (e) {
    $("#todolist").append(
      $("<li>").append([
        $("input").attr({ type: "checkbox" }).on("click", onClickCheckbox),
        $("<span>").text($("#todosubject").val()),
      ])
    );
  });

  const onClickCheckbox = function (e) {
    $(this)
      .parent()
      .toggleClass({ checked: $(this).val() });
    refreshCounter();
    refreshList();
  };

  const refreshCounter = function (e) {
    const doneCount = $("#todolist input:checked").length;
    const allCount = $("#todolist input").length;
    $("counter").text(`${doneCount} 件 / ${allCount} 件`);
  };

  const refreshList = /* 誰か作って!! */ ;
});
```

---

## jQuery

- ID (`#~~~`) とか多すぎ
  - 元々の HTML の構造に頼りながらコードを書いている
- おそらく追加仕様（ページングとか非表示とか）来ると死ぬ
- **UI に状態持てるのは単純なアプリまで**

---

## [React](https://ja.reactjs.org/)

![width:100px](images/react.png)

- `examples/todo-app/src/App.tsx`
- **宣言的 UI** という大正義
  - UI に状態があるのがまずいなら、UI に状態を持たなければいいじゃない！
  - `const UserInterface = (アプリの状態) => <UIの描画 />`
    - アプリの状態で完全に UI の描画が定まる関数を定義すれば終わりにしたい
  - React ならできるよ！
    - 仕組みとしては、アプリの状態が変わるたびに UI の描画を走らせて差分があるところだけを上手に実際に再描画するようになっている
- アプリの状態がまとまっていてクリーン
  - 状態管理ライブラリを使えばもっとクリーンにできる
  - 業務ルール的なクリーンな観点を持ち込みやすい
- 従来の HTML は他所にやる方法は正しかったか？
  - 新し目の手法だと HTML は処理とセットで手元に置く方が主流

---

## 別の UI ライブラリ

- [Preact](https://preactjs.com/)
  - React 互換で React より軽量
- [Svelte](https://svelte.jp/)
  - ビルド後のバンドルサイズが低いのがウリ
- [Vue.js](https://jp.vuejs.org/index.html)
  - 中国・日本で人気がある
- [Angular](https://angular.io/)
  - Google

---

## React の強み

- OSS / Meta 社が保守している
- HTML 部分もすべて JavaScript/TypeScript (jsx/tsx) で書けるためと親和性が高い
  - 他の UI ライブラリはテンプレート用に専用言語を用意しているせいでエディタサポートや周辺ツールサポートが甘い
- 新しく便利で強力な仕組みが導入されることが多い
  - [Algebraic Effects](https://overreacted.io/ja/algebraic-effects-for-the-rest-of-us/)
  - 反面アップデートがアグレッシブな場合も
- React のコンポーネントの思想は別プラットフォームでも通用する
  - [React Native](https://reactnative.dev/)
    - スマホアプリも React で開発できる
- フロントエンドつよつよな人が好むため、周辺ライブラリ・エコシステムも強い

---

## 状態管理ライブラリ

- React はあくまで UI 描画をヘルプするのがメインの役割であって、その他（以下）はあまりフォローしてくれない
  - **状態管理**
  - API サーバとの通信
  - コンポーネントのスタイリング
  - URL の振り分け（ルーティング）
- 状態管理ライブラリ: アプリの状態を管理するためのライブラリ
  - [Redux](https://redux.js.org/): `state := f(state, action)` で関数的に状態を更新するライブラリ
  - [MobX](https://mobx.js.org/): オブジェクトのプロパティを更新すると勝手に画面が再描画されるライブラリ
  - [recoil](https://recoiljs.org/): MobX と似てるが最近の React etc. への親和性が高い
- ただ「状態管理」の目的がほぼ、「API サーバとの通信」のキャッシュだったりするので、そこに狙いを付けたライブラリもある
  - [SWR](https://swr.vercel.app/ja)
  - [TanStack Query](https://tanstack.com/query/v4)

---

## まとめ

- 宣言的 UI への転向
  - 大きくて複雑なものを作る場合、命令的な UI 構築だと限界がくる
  - 新しい世代の UI ライブラリ(React など)はその点有利

## (参考) React の UI コンポーネントライブラリ

- [Chakra UI](https://chakra-ui.com/)
- [MUI](https://mui.com/)

---

<!-- _class: primary_section -->

# モダン Web フロントエンド開発 〜 アーキテクチャ編

MPA と SPA といったページのレンダリング手法について話します

---

## MPA vs. SPA - MPA: Multi-Page Application

```mermaid
sequenceDiagram
    participant ブラウザ
    participant Webサーバ
    ブラウザ->>Webサーバ: GET /index.php
    ブラウザ->>Webサーバ: GET /item/edit/index.php
    ブラウザ->>Webサーバ: POST /item/edit/confirm.php
```

- 昔ながらのページ遷移ごとに HTML をレンダリングして返すアプリ
- 既存のサイトはほとんどこれ（PHP など）
- 状態は HTML とセッションで持ち回る

---

## MPA vs. SPA - SPA: Single-Page Application

```mermaid
sequenceDiagram
    participant ブラウザ
    participant Webサーバ
    participant APIサーバ
    ブラウザ->>Webサーバ: GET /index.html
    ブラウザ->>APIサーバ: GET /api/v1/item
    ブラウザ->>APIサーバ: POST /api/v1/item
```

- 静的な HTML・JS・CSS のみで構成された **1 ページで全て完結**するアプリ
  - あらかじめビルドしたそれらを Web サーバにアップロードしてデプロイ完了
- 必要な後付のデータは全て API 通信 (ajax) で取得する
  - データの保存も API を通して行う
- **実際にページ遷移は行わずにページ遷移したかのように演出する**
  - ページの書き換えは全て JavaScript で行う
- **形を変えたファットクライアント**
- 状態は JS 中で保持すればよく、**セッションに依存する必要はない**

---

## SPA のメリット

- 静的なファットクライアント + API サーバにできるので、構成がシンプル
  - API 単位で認証・認可等を考慮すればいいのでセキュアにしやすい
- API 通信は基本的に JSON による電文になるため、構造化しやすく脆弱性やバグの排除に繋がりやすい
  - 複雑なデータを扱えない`<input>` 要素で一生懸命通信する必要はない
- UI の状態やデータの編集中の状態など全て JavaScript 上に保存するので、基本的にセッションを排除できる
  - セッション Cookie のかわりに [JWT](https://jwt.io/) などのトークンを使うことができる
- 画面側については JavaScript のみで構成することになるので、モダンフロントエンド（React など）の方法論が全面に適用しやすい
  - サーバサイド言語付随の変なテンプレートエンジンに頼る必要なし
    - **テンプレートエンジンから JavaScript に一生懸命データを渡す作業もする必要なし**
  - ネストした構造のデータを画面にロードするといったタスクが苦痛ではなくなる
- ページ遷移が軽量なので UX が良いことが多い

---

## SPA のメリット

- API サーバと開発を並行しやすい
- API サーバが別用途に流用しやすい
  - Web + スマホアプリの場合、どちらも共通の API サーバに通信するといったことが可能になる
  - 別に REST にこだわる必要はない
    - GraphQL
    - gRPC
    - JSON RPC

```mermaid
sequenceDiagram
    participant ブラウザ
    participant スマホアプリ
    participant 外部システム
    participant APIサーバ
    ブラウザ->>APIサーバ: GET /api/v1/my/data
    スマホアプリ->>APIサーバ: GET /api/v1/my/data
    外部システム->>APIサーバ: POST /api/v1/system/データ連携
```

---

## SPA のデメリット

- 古い弱いブラウザで読み込めない
  - [IE6 向けでもやろうと思えば SPA は作れます](https://qiita.com/boiyama/items/e10a1d52c232010e145e)
- ロードする JS のサイズが肥大化しがち
  - `GET /index.html` ＝ サイトの全機能の JS・CSS・関連リソース の読み込みになりがち
    - ロード戦略などによって回避可能ではあるが……
  - キャッシュ機構があるので 2 度目以降は速いことが多い
- 画面初期表示が遅くなりがち
  - ユーザーデータなど全て API で取得するため、HTML・JS の取得のあとに API 通信が発生・JS のレンダリング

```mermaid
sequenceDiagram
    participant ブラウザ
    participant Webサーバ
    participant APIサーバ
    ブラウザ->>Webサーバ: GET /index.html
    ブラウザ->>Webサーバ: GET /static/super_final_ultimate_overwhelming_gekiomo_bundle.js (10MB)
    ブラウザ->>APIサーバ: GET /api/v1/ようやく必要なデータのロード
```

---

## SPA のデメリット

- 割とブラウザページナビゲーションの JavaScript による実現が鬼門
  - 遷移後、戻るボタン後のスクロール位置が怪しい SPA 多数!!
  - 無限スクロールを実装してもナビゲーションで壊れる SPA 多数!!
- メモリリーク、状態残留への対処が面倒
  - MPA だとページ遷移すれば全部チャラになっていたものが残留

---

## 業務系は割と SPA が向いている

- 凝った編集画面多め
- 画面の初期表示（ファーストペイン）速度への要件緩め
- 決まったユーザ・決まった利用目的
- 開発体制として画面・API が綺麗に分かれていたほうが作りやすい
  - API 仕様の共有: [OpenAPI Specification](https://swagger.io/specification/)

---

## 更に高度な戦術

- サーバで画面をレンダリングする
  - ファーストペインの改善・SEO
  - CSR: Client Side Rendering
    - 従来の SPA の描画方法
  - SSR: Server Side Rendering
    - 画面の初期描画をサーバで済ませる
  - ISR: Incremental Static Regeneration
    - 画面の描画をキャッシュし、裏で再生成する
  - React Server Components
    - API 通信せずにコンポーネントの描画をサーバで行う
- PWA: Progressive Web Apps

---

## いろいろ面倒なあなたに [Next.js](https://nextjs.org/) の紹介

![width:100px](images/nextjs.png)

- ここまで色々説明してきたが、「あー、色々多い　面倒くさっ」感が強い
- そこで、下記を設定ゼロでまとめて提供してくれる [Next.js](https://nextjs.org/) というのがある
  - React
  - TypeScript
  - webpack
  - Babel
  - CSR, SSR, ISR
  - API サーバ
  - ルーティング
- 今日では React のプロジェクトはかなり Next.js で作られていることが多い
- デカくなりがちな SPA のモジュールの配信も何も考えず色々最適になることが多い
  - SPA で都合が悪くなった時に SSR などに逃げることもできる

---

<!-- _class: primary_section -->

# モダン Web フロントエンド開発 〜 リアル開発編

案件の実例を紹介していきます

---

## 案件 1

- フロントエンド
  - 言語: ClojureScript
  - UI: React
    - 状態管理: [re-frame](https://github.com/day8/re-frame)
- サーバサイド
  - 言語: Ruby
  - フレームワーク: Ruby on Rails

---

## 案件 2, 3

- フロントエンド
  - 言語: TypeScript
  - UI: React
    - 状態管理: mobx-state-tree
- サーバサイド
  - 言語: Kotlin
  - フレームワーク: Spring Framework
- API 仕様: Open API Specification

---

## 案件 4

- フロントエンド
  - 言語: TypeScript
  - UI: React
    - 状態管理: mobx-keystone
- バックエンド
  - 言語: TypeScript
  - フレームワーク: Next.js
- Next.js で一つのプロジェクトとしてフロント〜バックエンドを構成
- API 仕様: [aspida](https://github.com/aspida/aspida)

---

## 全体的な開発構成

- 画面開発 <=> API 開発 で並列して開発
  - 何らかの形で API のスキーマは画面と API 側で共有できるようにする
- 画面（フロントエンド）は Visual Studio Code で開発
- ローカル環境で出来上がった API サーバを結合しながら開発
  - API サーバのモックを用意してもいいが、工数的に無駄に違いはないので、API の方が開発完了が先行するように

## 参考になる方法論

- [オブジェクト指向 UI デザイン ── 使いやすいソフトウェアの原理 (WEB+DB PRESS plus シリーズ)](https://www.amazon.co.jp/%E3%82%AA%E3%83%96%E3%82%B8%E3%82%A7%E3%82%AF%E3%83%88%E6%8C%87%E5%90%91UI%E3%83%87%E3%82%B6%E3%82%A4%E3%83%B3%E2%94%80%E2%94%80%E4%BD%BF%E3%81%84%E3%82%84%E3%81%99%E3%81%84%E3%82%BD%E3%83%95%E3%83%88%E3%82%A6%E3%82%A7%E3%82%A2%E3%81%AE%E5%8E%9F%E7%90%86-WEB-DB-PRESS-plus%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA/dp/4297113511)
  - 近代的な UI 構築の考え方がわかる
  - 「照会画面」「編集画面」「確認画面」の構成に違和感がない人は特に読んだほうがいい

---

<!-- _class: primary_section -->

# まとめ

本日のまとめです

---

## まとめ

- 画面開発は JavaScript が中心の時代
- 大規模かつ仕様が難解な現代の Web にはふさわしい開発方法がある
  - TypeScript, React, webpack, SPA など

## 参考図書

- [TypeScript と React/Next.js でつくる実践 Web アプリケーション開発](https://www.amazon.co.jp/TypeScript%E3%81%A8React-Next-js%E3%81%A7%E3%81%A4%E3%81%8F%E3%82%8B%E5%AE%9F%E8%B7%B5Web%E3%82%A2%E3%83%97%E3%83%AA%E3%82%B1%E3%83%BC%E3%82%B7%E3%83%A7%E3%83%B3%E9%96%8B%E7%99%BA-%E6%89%8B%E5%B3%B6-%E6%8B%93%E4%B9%9F/dp/4297129167)
  - 細部の正確さが抜けているが、とりあえずとっかかりとして割り切って読むなら悪くない本
  - 部で輪読会済

---

<!-- _class: primary_section -->

# 質疑応答

---

## Q. TODO リストへの検索機能追加について見たかった

数分で実装できました。（下記 diff）

```diff
--- a/examples/todo-app/src/App.tsx
+++ b/examples/todo-app/src/App.tsx
@@ -7,16 +7,24 @@ function App() {
   // アプリ の状態
   const [todoList, setTodoList] = useState<Todo[]>([]);
   const [filterDone, setFilterDone] = useState(false);
+  const [filterText, setFilterText] = useState("");
   const [todoSubject, setTodoSubject] = useState("");

   // 派生状態
   const todoListForDisplay = sortBy(
-    todoList.filter((it) => {
-      if (filterDone) {
-        return !it.done;
-      }
-      return true;
-    }),
+    todoList
+      .filter((it) => {
+        if (filterText) {
+          return it.subject.includes(filterText);
+        }
+        return true;
+      })
+      .filter((it) => {
+        if (filterDone) {
+          return !it.done;
+        }
+        return true;
+      }),
     (it) => -it.createdAt.getTime()
   );

@@ -80,6 +88,12 @@ function App() {
             {todoList.filter((it) => it.done).length} 件 / {todoList.length}{" "}
             件）
           </label>
+          <input
+            className="border rounded p-2 w-full"
+            placeholder="タイトルで絞り込みたい場合はここに入力してください"
+            value={filterText}
+            onChange={({ currentTarget: { value } }) => setFilterText(value)}
+          />
         </div>
       </div>
```

---

## Q. React の使いどころ

- CMS に使えるのか？
  - 何らかのベースがすでにある場合（WordPress など）、使いづらいと思います
  - ウェブページにアドホックに小さな機能を足すのにはあまり向いていないです
    - こういうのは jQuery のほうが向いているはずです
  - ただあまりにも込み入った画面部品を製造するなら、それに向けて使うことはあります
- 使いどころ
  - 画面とは別に API サーバがある場合
    - ヘッドレス(画面なし)CMS というのがあって、CMS 自体が API サーバとなっているものも作れます
      - https://techblog.zozo.com/entry/microcms-zozotown-usecase
      - https://zenn.dev/nekoniki/articles/964696fae52f2d
  - 開発チームが React に慣れている場合
- 結局のところ、このスライドで紹介したような画面開発を JavaScript で頭からしっぽまで一貫して開発する以外の場合は、ややポテンシャルが発揮できないかなと思います

---

## Q. SPA を設置する場合のインフラ構成

- 今回フロントエンドの話題なので、特に API サーバの実装詳細は対象にはしていませんが、次ページにイメージを書いておきました
- Web サーバ
  - 静的コンテンツを返せるようなサーバであれば Nginx などなんでも構いません。
  - 単純にファイル配信するだけなので機能的である必要はありません。
- API サーバ
  - フォームからの入力や HTML の出力の代わりに JSON を入出力します。
    - JSON を処理して DB に永続化する機能（業務処理）は API サーバ上に実装しなければいけません。
    - 下記は SPA 側で行います
      - 画面入力から API サーバに送りつける JSON を作成
      - API サーバから返ってきた JSON を解釈して画面表示に反映する
  - 従来どおりの MPA が稼働しているような Web サーバと同様のインフラ構成で構いません。
    - やることといったら、 HTTP/HTTPS で HTTP リクエストを受け付けるだけなので
  - 上記の Web サーバと API サーバは同居していても問題ないです。
    - [CORS](https://developer.mozilla.org/ja/docs/Web/HTTP/CORS) を考慮すると同居したほうがシンプルです。

---

```mermaid
sequenceDiagram
    participant ブラウザ
    participant Webサーバ
    participant APIサーバ
    participant DBサーバ
    ブラウザ->>Webサーバ: HTML/CSS/JS DLするだけ
    ブラウザ->>APIサーバ: JSON電文
    APIサーバ->>DBサーバ: SQLクエリなどでデータ取り出し・永続化
```

（詳しく知りたい場合はコンタクトをとってください）

---

## Q. IE/ActiveX の代替手段について

- 特に詳しい分野ではありませんが、直接の代替手段はありません
- 込み入ったブラウザ上での制御が必要な場合は、たとえば下記のような手段で逃げることになると思います。
  - Chrome などブラウザの拡張機能を独自で作成する
  - Electron や WebView などブラウザを基盤にしたソフトウェア上でサイトを表示しつつ、独自拡張をサイト内からアドホックに呼べるようにしておく
  - 連携ソフトウェア側に nonce など安全なトークン付き URL を連携し、データを連携ソフトウェア側で別途取得してもらう
- 長い `mailto:` を実現する方法は調べた限りなさそうです

---

# おわり
