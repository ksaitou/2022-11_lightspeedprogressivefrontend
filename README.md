# 2022-11 フロントエンドキャッチアップ会

- スライド: https://ksaitou.gitlab.io/2022-11_lightspeedprogressivefrontend/
- リポジトリ: https://gitlab.com/ksaitou/2022-11_lightspeedprogressivefrontend

## スライドの作り方

### スライドの開発

```
# 依存性取得
$ yarn

# サーバ: http://localhost:8080
$ yarn start

# HTMLで出力
$ yarn build
```

### 参考

- marp (プレゼンツール) - https://github.com/marp-team/marp-cli
  - https://marpit.marp.app/markdown
