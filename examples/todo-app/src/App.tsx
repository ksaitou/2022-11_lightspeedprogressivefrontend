import { useState } from "react";
import "./index.css";
import dayjs from "dayjs";
import { sortBy } from "lodash";

function App() {
  // アプリ の状態
  const [todoList, setTodoList] = useState<Todo[]>([]);
  const [filterDone, setFilterDone] = useState(false);
  const [filterText, setFilterText] = useState("");
  const [todoSubject, setTodoSubject] = useState("");

  // 派生状態
  const todoListForDisplay = sortBy(
    todoList
      .filter((it) => {
        if (filterText) {
          return it.subject.includes(filterText);
        }
        return true;
      })
      .filter((it) => {
        if (filterDone) {
          return !it.done;
        }
        return true;
      }),
    (it) => -it.createdAt.getTime()
  );

  // イベントハンドラ
  const addToTodoList = () => {
    setTodoList([
      ...todoList,
      {
        id: `${Math.random()}`,
        subject: todoSubject,
        done: false,
        createdAt: new Date(),
      },
    ]);
    setTodoSubject("");
  };
  const switchDone = ({ id }: Todo) => {
    setTodoList(
      todoList.map((it) => {
        if (it.id === id) {
          return {
            ...it,
            done: !it.done,
          };
        }
        return it;
      })
    );
  };

  return (
    <div className="flex flex-col">
      <div className="border-b pb-4 flex items-center">
        <div className="p-2">
          <form className="" method="dialog">
            <input
              className="border rounded p-2 w-80"
              type="text"
              placeholder="Todoタイトル"
              value={todoSubject}
              onChange={({ currentTarget: { value } }) => setTodoSubject(value)}
            />
            <button
              className="bg-slate-600 disabled:bg-slate-400 text-white py-2 px-4"
              onClick={addToTodoList}
              disabled={todoSubject.length === 0}
            >
              追加する
            </button>
          </form>
          <label>
            <input
              className="mr-2"
              type="checkbox"
              checked={filterDone}
              onChange={({ currentTarget: { checked } }) =>
                setFilterDone(checked)
              }
            />
            チェック済みのものは表示から除外する （完了:{" "}
            {todoList.filter((it) => it.done).length} 件 / {todoList.length}{" "}
            件）
          </label>
          <input
            className="border rounded p-2 w-full"
            placeholder="タイトルで絞り込みたい場合はここに入力してください"
            value={filterText}
            onChange={({ currentTarget: { value } }) => setFilterText(value)}
          />
        </div>
      </div>

      <div className="grow overflow-scroll">
        <TodoList todoList={todoListForDisplay} onSwitchDone={switchDone} />
      </div>
    </div>
  );
}

const TodoList = ({
  todoList,
  onSwitchDone,
}: {
  todoList: Todo[];
  onSwitchDone: SwitchDoneHandler;
}) => {
  if (todoList.length === 0) {
    return <div>表示するToDoはありません</div>;
  }

  return (
    <ul>
      {todoList.map((it) => (
        <TodoItem key={it.id} todo={it} onSwitchDone={onSwitchDone} />
      ))}
    </ul>
  );
};

const TodoItem = ({
  todo,
  onSwitchDone,
}: {
  todo: Todo;
  onSwitchDone: SwitchDoneHandler;
}) => {
  return (
    <li
      className={`p-2 flex items-center cursor-pointer ${
        todo.done ? "line-through" : ""
      }`}
      onClick={() => onSwitchDone(todo)}
    >
      <input className="mr-2" type="checkbox" checked={todo.done} />
      <div>
        <span>{todo.subject}</span>
        <time className="block text-xs">
          {dayjs(todo.createdAt).format("YYYY-MM-DD HH:mm:ss")}
        </time>
      </div>
    </li>
  );
};

type SwitchDoneHandler = (item: Todo) => void;

type Todo = {
  id: string;
  subject: string;
  done: boolean;
  createdAt: Date;
};

export default App;
